#include <stdfix.h>
#include <iostream>
using namespace std;

int main() {

    /* When we declare a variable, memory is reserved. The
     * number of bytes is determined by the type of variable.
     * The location in memory is not of concern to us. We
     * simply use the variable name to refer to it.
     */
    int num = 3;

    /* A pointer is a variable which holds the memory address
     * of an object.
     */
    int* pNum = &num;

    /* The asterisk marks the variable as a pointer to an 
     * int. It is itself not necessarily an int, just the
     * address of one. The ampersand is an operator that 
     * returns the address of a varable, rather than its
     * value.
     */

    /* The value may vary on subsequent runs of the 
     * program, because the variable will be located at a
     * different place in memory.
     */
    cout << pNum << endl;

    /* Getting the value stored at the address save in a 
     * pointer is called dereferencing. When dereferencing
     * a pointer, the asterisk (*) is used.
     */
    cout << *pNum << endl;

    /* The same operator can be used to change the value of
     * the referenced variable. Because we are changing the 
     * value actually stored at that memory address, the 
     * original variable's value is changed as well.
     */
    *pNum = 44;
    cout << num << endl;

    /* ALWAYS INITIALIZE POINTERS
     * If there is nothing to initialize it to, use NULL,
     * nullptr, or zero. Uninitialized pointers can cause
     * serious bugs that may be difficult to track down.
     */
    int* pNum2; // BAD
    cout << pNum2 << endl;

    return 0;


}