#include <iostream>
using namespace std;

void addOneValue(int);
void addOneRef(int&);

int main() {

    /* References
     * A reference is an alias for another type. The apersand
     * is used to denote a reference variable. 
     */
    int num = 5;
    int &refNum = num; // references the same memory location
    // int &refNumUninit; // uninitialized, DON'T
    cout << refNum << endl; // outputs 5

    // note that the reference and the orignal variable are
    // using the same memory address.                                  
    cout << &num << endl;
    cout << &refNum << endl;

    // Passing by reference is often used in functions, since
    // passing variables into functions will simply make a
    // copy unrelated to the original, by default. 
    addOneValue(num);
    cout << num << endl;

    // Passing by reference allows the original variable
    // to be modified, rather than an ephemeral copy.
    addOneRef(refNum);
    cout << num  << endl;

}

// the local variable x is incremented, but the original
// is unchanged. x is a copy, which was given the same value
// of the passed variable. x is incremented, then goes out 
// of scope and is destroyed.
void addOneValue(int x) { x++; }

// the variable x has the same memory address as the variable
// that was passed, so changes to it will also affect the 
// original. 
void addOneRef(int &x) { x++; }