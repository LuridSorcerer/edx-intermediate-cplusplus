#include <iostream>
using namespace std;

int main() {

/* Allocating memory during an application's run is common. Allocating
 * memory to new objects is done using pointers. 
 */

    // declare a pointer to an int and reserve memory for it
    int *pInt = new int;

    // declare a pointer to a double and reserve memory for it
    double *pDouble = new double;

    // store value of 3 into memory pointed to by int pointer
    *pInt = 3;

    // store value of 4.2 in memory pointed to by double pointer
    *pDouble = 4.2;

    cout << "At address " << pInt << " is the value " << *pInt << endl;
    cout << "At address " << pDouble << " is the value " << *pDouble << endl;

    // when finished with dynamically allocated memory, it is important
    // to release it back to the OS. This is done using the delete
    // keyword.
    delete pInt;
    delete pDouble;

    return 0;

}