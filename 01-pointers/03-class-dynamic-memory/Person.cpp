#include <iostream>
#include "Person.h"

Person::Person() { }

Person::Person(string fname, string lname) {
    firstName = fname;
    lastName = lname;
}

Person::Person(string fname, string lname, int age) {
    firstName = fname;
    lastName = lname;
    age = age;
}

Person::~Person() {
    std::cout << "Person destructor called" << std::endl;
}

void Person::setFirstName(string fname) {firstName = fname; }
string Person::getFirstName() { return firstName; }

void Person::setLastName(string lname) { lastName = lname; }
string Person::getLastName() { return lastName; }

void Person::setAge(int age) { age = age; }
int Person::getAge() { return age; }

void Person::sayHello() { ; }