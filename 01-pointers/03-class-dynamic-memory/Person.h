#include <string>
using namespace std;

class Person
{
    private:
        string firstName;
        string lastName;
        int age;

    public:
        Person();

        Person(string fname, string lname);

        Person(string fname, string lname, int age);

        ~Person();

        void setFirstName(string fname);
        string getFirstName();

        void setLastName(string lname);
        string getLastName();

        void setAge(int age);
        int getAge();

        void sayHello();
};