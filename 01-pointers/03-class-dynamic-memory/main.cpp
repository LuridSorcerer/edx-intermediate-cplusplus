#include <iostream>
#include "Person.h"
using namespace std;

int main() {
    
    Person *pOne = new Person("Solid","Snake",40);

    cout << "First name: " << pOne->getFirstName() << endl;
    cout << "Last name: " << pOne->getLastName() << endl;
    cout << "Memory address: " << &pOne << endl;

    delete pOne;

    return 0;
}