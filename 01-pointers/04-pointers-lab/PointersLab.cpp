#include <iostream>
#include <string>
using namespace std;

class Person {
    public:

        // todo: make these private and add accessing functions
        string name;
        int age;
        int height; // in pounds
        int weight; // in inches
};

void PassByValue(int);
void PassByRef(int&);
void ModifyPerson(Person&);

int main() {

    int num1;
    int *pNum = new int;

    num1 = 3;

    *pNum = 5;

    PassByValue(num1);
    cout << "value of num1 after PassByValue: " << num1 << endl;

    PassByRef(*pNum);
    cout << "value pointed to by pNum after PassByRef: " << *pNum << endl;

    //PassByValue(pNum);
    // Error: argument of type 'int*' incompatible with type
    // 'int'.
    // Would work if dereferenced:
    // PassByValue(*pNum);

    delete pNum;

    double *pDubs = new double;

    *pDubs = 7.938;

    cout << "Double value is: " << *pDubs << endl;
    delete pDubs;
    cout << "Double value after deallocation is: " << *pDubs << endl;
    // Expected some kind of use-after-free error, but it just
    // returned the value zero. No error. That is to say, it is
    // certainly a logical error, but the program didn't crash.

    Person examplePerson {"Duke Nukem",40,74,180};
    cout << "Name: " << examplePerson.name << endl
         << "Age: " << examplePerson.age << endl
         << "Height: " << examplePerson.height / 12 << "' " << examplePerson.height % 12 << "\"" << endl
         << "Weight: " << examplePerson.weight << " lbs." << endl;

    ModifyPerson(examplePerson);
    cout << "Changed name: " << examplePerson.name << endl;
    
    return 0;

}

void PassByValue(int x) {
    x++;
    cout << "PassByValue: new value is " << x << endl;
    return;
}

void PassByRef(int &x) {
    x = 50;
    cout << "PassByRef: new value is " << x << endl;
}

void ModifyPerson(Person &p) {
    p.name = "Raphael";
}