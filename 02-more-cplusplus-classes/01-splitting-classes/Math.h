#pragma once

/* Sample code shows this class as being static. My research suggests
 * this is not correct in C++. A class can have strictly static
 * methods, making it in essence a static class, but this feature is
 * not valid in C++ as it is in Java and C#.
 */
/*static*/ class Math 
{

    public:

    /* Sample code shows the namespace defined here. I get a compiler
     * error, and it is redundant.
     */
    static int /*Math::*/pow(int base, int exp);

};
