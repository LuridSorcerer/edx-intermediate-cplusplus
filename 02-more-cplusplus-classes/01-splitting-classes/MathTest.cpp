//#include "stdafx.h"
#include "Math.h"
#include "Person.h"
#include <iostream>
using namespace std;

int main()
{
    // test our pow() function
    int result = Math::pow(2,10);
    cout << result << endl;

    // demonstrate the three constructors
    Person *pOne = new Person();
    Person *pTwo = new Person("Duke","Nukem");
    Person *pThree = new Person("Johnny","Quest",348);

    pOne->SayHello();

    // release the memory back to the OS
    delete pOne;
    delete pTwo;
    delete pThree;

    return 0;
}