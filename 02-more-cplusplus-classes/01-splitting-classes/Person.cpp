//#include "stdafx.h"
#include "Person.h"
#include <iostream>

Person::Person() {

}

Person::Person(std::string fName, std::string lName) {
    this->firstName = fName;
    this->lastName = lName;    
}

Person::Person(std::string fName, std::string lName, int age) {
    this->firstName = fName;
    this->lastName = lName;
    this->age = age;
}

Person::~Person() {

}

void Person::SetFirstName(std::string fName) {
    this->firstName = fName;
}

std::string Person::GetFirstName() { return firstName; }

void Person::SetLastName(std::string lName) {
    this->lastName = lName;
}

std::string Person::GetLastName() { return lastName; }

void Person::SetAge(int age) {
    this->age = age;
}

int Person::GetAge() { return age; }

void Person::SayHello() {
    std::cout << "Hello" << std::endl;
}