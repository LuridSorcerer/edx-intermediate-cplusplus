//#include "stdafx.h"
#include "Dog.h"
#include <iostream>

Dog::Dog() {

}

Dog::Dog(std::string fName, std::string lName) {
    this->firstName = fName;
    this->lastName = lName;    
}

Dog::Dog(std::string fName, std::string lName, int age) {
    this->firstName = fName;
    this->lastName = lName;
    this->age = age;
}

Dog::~Dog() {

}

void Dog::SetFirstName(std::string fName) {
    this->firstName = fName;
}

std::string Dog::GetFirstName() { return firstName; }

void Dog::SetLastName(std::string lName) {
    this->lastName = lName;
}

std::string Dog::GetLastName() { return lastName; }

void Dog::SetAge(int age) {
    this->age = age;
}

int Dog::GetAge() { return age; }

/* Even though the name matches a function in Person, the functionality
 * is different. The compiler selects the correct method by using 
 * class scope.
 */
void Dog::SayHello() {
    std::cout << "Woof" << std::endl;
}