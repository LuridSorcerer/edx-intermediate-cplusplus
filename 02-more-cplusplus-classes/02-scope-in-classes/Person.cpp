//#include "stdafx.h"
#include "Person.h"
#include <iostream>

Person::Person() {

}

Person::Person(std::string fName, std::string lName) {
    this->firstName = fName;
    this->lastName = lName;    
}

Person::Person(std::string fName, std::string lName, int age) {
    this->firstName = fName;
    this->lastName = lName;
    this->age = age;
}

Person::~Person() {

}

void Person::SetFirstName(std::string fName) {
    this->firstName = fName;
}

std::string Person::GetFirstName() { return firstName; }

void Person::SetLastName(std::string lName) {
    this->lastName = lName;
}

std::string Person::GetLastName() { return lastName; }

/* Thanks to encapsulation, we do not have direct access to the age 
 * variable. Because of this, the age can only be changed by member
 * methods, allowing us to validate values before they are applied.
 */
void Person::SetAge(int age) {
    if (age < 0) {
        std::cout << "Age must not be negative" << std::endl;
    } else {
        this->age = age;
    }
}

int Person::GetAge() { return age; }

void Person::SayHello() {
    std::cout << "Hello" << std::endl;
}