#include <iostream>
#include "Dog.h"
#include "Person.h"

using namespace std;

int main() {

    Person *person = new Person("Ted","Roosevelt",55);

    Dog *dog = new Dog("Sprinkles","the Dog", 3);

    person->SayHello();
    dog->SayHello();

    cout << "Person starting age: " << person->GetAge() << endl;
    cout << "Setting age to -4: ";
    person->SetAge(-4);
    cout << "Person age now: " << person->GetAge() << endl;

    delete person;
    delete dog;

    return 0;

}