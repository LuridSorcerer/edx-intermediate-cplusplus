#include "Student.h"

Student::Student() { }

Student::Student(string fName, string lName,
    int age, string address, string city,
    string phoneNumber) {
    this->firstName = fName;
    this->lastName = lName;
    this->age = age;
    this->address = address;
    this->city = city;
    this->phoneNumber = phoneNumber;
}

Student::~Student() { }

void Student::SetFirstName(string fName) {
    firstName = fName;
}

string Student::GetFirstName() { return firstName; }

void Student::SetLastName(string lName) {
    lastName = lName;
}

string Student::GetLastName() { return lastName; }

void Student::SetAge(int new_age) {
    age = new_age;
}

int Student::GetAge() { return age; }

void Student::SetAddress(string new_address) {
    address = new_address;
}

string Student::GetAddress() { return address; }

void Student::SetCity(string new_city) {
    city = new_city;
}

string Student::GetCity() { return city; }

void Student::SetPhoneNumber(string new_number) {
    phoneNumber = new_number;
}

string Student::GetPhoneNumber() {
    return phoneNumber;
}

void Student::SitInClass() {
    cout << "Sitting in main theater" << endl;
}