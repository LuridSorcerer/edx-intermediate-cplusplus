#pragma once

#include <iostream>
#include <string>
using namespace std;

class Student {

private:
    string firstName;
    string lastName;
    int age;
    string address;
    string city;
    string phoneNumber;

public:
    Student();
    Student(string fName, string lName, int age, 
        string address, string city, string phoneNum);
    ~Student();

    void SetFirstName(string fName);
    string GetFirstName();

    void SetLastName(string lName);
    string GetLastName();

    void SetAge(int age);
    int GetAge();

    void SetAddress(string address);
    string GetAddress();

    void SetCity(string city);
    string GetCity();

    void SetPhoneNumber(string phoneNumber);
    string GetPhoneNumber();

    void SitInClass();

};