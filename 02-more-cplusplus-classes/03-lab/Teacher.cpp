#include "Teacher.h"

Teacher::Teacher() { }

Teacher::Teacher(string fName, string lName,
    int age, string address, string city,
    string phoneNumber) {
    this->firstName = fName;
    this->lastName = lName;
    this->age = age;
    this->address = address;
    this->city = city;
    this->phoneNumber = phoneNumber;
}

Teacher::~Teacher() { }

void Teacher::SetFirstName(string fName) {
    firstName = fName;
}

string Teacher::GetFirstName() { return firstName; }

void Teacher::SetLastName(string lName) {
    lastName = lName;
}

string Teacher::GetLastName() { return lastName; }

void Teacher::SetAge(int new_age) {
    age = new_age;
}

int Teacher::GetAge() { return age; }

void Teacher::SetAddress(string new_address) {
    address = new_address;
}

string Teacher::GetAddress() { return address; }

void Teacher::SetCity(string new_city) {
    city = new_city;
}

string Teacher::GetCity() { return city; }

void Teacher::SetPhoneNumber(string new_number) {
    phoneNumber = new_number;
}

string Teacher::GetPhoneNumber() {
    return phoneNumber;
}

void Teacher::GradeStudent() {
    cout << "Student graded" << endl;
}

void Teacher::SitInClass() {
    cout << "Sitting in front of class" << endl;
}