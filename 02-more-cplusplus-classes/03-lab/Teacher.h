#pragma once

#include <iostream>
#include <string>
using namespace std;

class Teacher {

private:
    string firstName;
    string lastName;
    int age;
    string address;
    string city;
    string phoneNumber;

public:
    Teacher();
    Teacher(string fName, string lName, int age, 
        string address, string city, string phoneNum);
    ~Teacher();

    void SetFirstName(string fName);
    string GetFirstName();

    void SetLastName(string lName);
    string GetLastName();

    void SetAge(int age);
    int GetAge();

    void SetAddress(string address);
    string GetAddress();

    void SetCity(string city);
    string GetCity();

    void SetPhoneNumber(string phoneNumber);
    string GetPhoneNumber();

    void GradeStudent();

    void SitInClass();

};