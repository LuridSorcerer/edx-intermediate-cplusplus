#include <iostream>
#include "Teacher.h"
#include "Student.h"
#include "Course.h"

using namespace std;

int main() {

    // 1. Instantiate three students...
    Student* Student1 = new Student();
    Student* Student2 = new Student();
    Student* Student3 = new Student();

    // 2. Instantiate course...
    Course* IntermediateCpp = new Course();

    // 3. Add your three students to this course object
    IntermediateCpp->students[0] = *Student1;
    IntermediateCpp->students[1] = *Student2;
    IntermediateCpp->students[2] = *Student3;

    // 4. Instantiate at least one Teacher object
    Teacher* AtLeast = new Teacher();

    // 5. Add that Teacher object to your Course object
    IntermediateCpp->teacher = *AtLeast;

    // 6. Do the following:
        // 1. Output of the course
        IntermediateCpp->name = "Intermediate C++";
        cout << "The course is: " << IntermediateCpp->name << endl;

        // 2.  Call GradeStudent() on Teacher object
        AtLeast->GradeStudent();

        // 3. Leave application open and answer Lab assessment questions

    delete AtLeast;
    delete Student1;
    delete Student2;
    delete Student3;
    delete IntermediateCpp;      

    return 0;

}