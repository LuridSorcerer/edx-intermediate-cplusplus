#include "Student.hpp"
#include <iostream>

Student::Student() {
    // watch for the base class's default contructor getting called as well before this
    std::cout << "Student::Student() called" << std::endl;
}

Student::~Student() {
    std::cout << "Student::~Student() called" << std::endl;
}