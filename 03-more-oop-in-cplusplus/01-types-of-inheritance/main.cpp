#include <iostream>
#include "Person.hpp"
#include "Student.hpp"

using namespace std;

int main() {

    Student student1;

    // firstName is inherited from Person, but is private,
    // so it cannot be directly accessed.
    // A compiler error will result.
    //student1.firstName = "tom";

    // SayHello() was inherited from Person. As a public
    // method, it can be called without issue.
    student1.SayHello();

    return 0;

}