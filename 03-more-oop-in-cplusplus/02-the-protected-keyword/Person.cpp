#include "Person.hpp"
#include <iostream>

Person::Person() {
    std::cout << "Person::Person() called" << std::endl;
}

Person::Person(std::string fName, std::string lName){
    firstName = fName;
    lastName = lName;
}

Person::Person(std::string fName, std::string lName, int age){
    firstName = fName;
    lastName = lName;
    this->age = age;
}

Person::~Person(){
    std::cout << "Person::~Person() called" << std::endl;
}

void Person::SayHello() {
    std::cout << "Hello!" << std::endl;
}