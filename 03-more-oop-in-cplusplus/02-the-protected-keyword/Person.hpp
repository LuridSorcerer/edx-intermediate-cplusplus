#pragma once

#include <string>

class Person {

// only accessable inside class methods
private:
    std::string firstName;
    std::string lastName;

// only accessible in class and derrived class methods
protected:
    int age;

// accessable anywhere
public:
    Person();

    Person(std::string fName, std::string lName);

    Person(std::string fName, std::string lName, int age);

    ~Person();

    void SayHello();

};