#pragma once

#include "Person.hpp"
#include <string>

class Student: public Person {

private:
    std::string course;

public:
    Student();
    ~Student();

};