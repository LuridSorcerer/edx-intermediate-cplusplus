/* The protected Keyword
 * Public members of a class are accessable outside of that
 * class. Private members are only accessable inside that 
 * class. 
 * 
 * Protected members are private outside of the class, but
 * public inside any derrived class.
 */

#include <iostream>
#include "Person.hpp"
#include "Student.hpp"

int main() {
    
}