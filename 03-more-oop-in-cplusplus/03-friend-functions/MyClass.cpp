#include "MyClass.hpp"
#include <iostream>

MyClass::MyClass() {
    num = 55;
}

MyClass::~MyClass() {

}

// Leave off the class identifier because friend functions
// are not actually members of the class.
// The passed-in reference's private members are available, but
// the this keyword is unavailable because the function is actually
// not a member inside the class.
void SomeExternalFunction(MyClass & myObject) {
    std::cout << "SomeExternalFunction() called" << std::endl;
    std::cout << "Private member num is: " << myObject.num << std::endl;
}