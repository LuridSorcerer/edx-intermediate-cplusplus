class MyClass {

    // friend functions can be called outside of the class and 
    // may access private class members. However, they require
    // a reference to the object to be passed in.s
    friend void SomeExternalFunction(MyClass & targetObject);

private:

    int num;

public:

    MyClass();

    ~MyClass();
};