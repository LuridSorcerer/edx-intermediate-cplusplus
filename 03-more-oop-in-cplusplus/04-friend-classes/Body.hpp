#pragma once

class Handle;

class Body {
    friend class Handle;

// Even though this member is private, the Handle class will be 
// able to access it.
private:
    int data;

};