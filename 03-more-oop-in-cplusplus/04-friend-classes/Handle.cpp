#include "Handle.hpp"
#include <iostream>

Handle::Handle() {
    // create underlying Body object
    body = new Body;
    body->data = 485;
}

Handle::~Handle() {
    // free up underlying Body object
    delete body;
}

void Handle::ShowBodyData() {
    std::cout << "Body data is: " << body->data << std::endl;
}