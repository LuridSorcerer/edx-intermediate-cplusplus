#pragma once

#include "Body.hpp"

class Handle {

private:
    // the "handle" class typically maintains an internal
    // instance of the "body" class.
    Body* body;

public:
    Handle();
    ~Handle();

    void ShowBodyData();

};