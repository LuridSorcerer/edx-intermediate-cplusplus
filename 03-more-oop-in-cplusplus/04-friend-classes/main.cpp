/* A class can define another class as its friend.
 * Friend classes can access all the members of the
 * first class. 
 * 
 * One example of friend classes is the "handle-body" 
 * idiom. In this case, one class is the "body", and 
 * contains all the private data. The other class is 
 * the "handle", which contains all of the public 
 * behavior for interacting with the body data.
 */

#include <iostream>
#include "Handle.hpp"
using namespace std;

int main() {
    Handle handle;
    handle.ShowBodyData();

    // Though a Handle object can directly access the private members of its 
    // Body object, the Body object's private members cannot be directly
    // accessed outside of either class. The following code generates
    // a compiler error.
    //
    // Body body;
    // cout << "Trying to access it directly: " << body.data << endl;

    return 0;
}